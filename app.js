var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var uploadRouter = require('./routes/upload')
const session = require('express-session')
const log4js = require('./logCofig')
const log = log4js.getLogger()// 根据需要获取logger
const logErr = log4js.getLogger('err')

var app = express()

// 结合express使用，记录请求日志
log4js.useLogger(app, log)// 这样会自动记录每次请求信息，放在其他use上面

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// 使用session
app.use(session({
  secret: 'sbtK24E6Fa',
  cookie: { maxAge: 60000 * 30 }}))

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/upload', uploadRouter)
app.use('/users', usersRouter)
app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  logErr.error(err)
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
