const express = require('express')
const router = express.Router()
const mongoPromise = require('../modules/promise')
const httpCom = require('../modules/httpCommon')
const multer = require('multer')
const file = require('../modules/mongo/upload/file').File

// 实例化multer并设置路径和过滤器
const mult = multer({
    dest: 'files/test/',
    fileFilter: (req, file, cb) => {
        cb(null, true)
    },
    limits: {
        fileSize: '20MB',
        files: 1
    }
})
router.post('/file',mult.single('myfile'), async (req, res, next) => {
  console.log(req.file)

  const resData = await mongoPromise.insert(file, {url: file.path}).catch( err => {
    console.error(err)
    res.send(httpCom.getRsp(1))
  })
  if(resData) res.send(httpCom.getRsp(200))
  res.end()
})
module.exports = router