var express = require('express')
var User = require('../modules/mongo/user/user').User
const mongoPromise = require('../modules/promise')
const Auth = require('../modules/auth/passw')
const httpCom = require('../modules/httpCommon')

var router = express.Router()

/* GET users listing. */
router.post('/register',async (req, res) => {
  if(req.ip.indexOf('127.0.0.1') === -1) res.header('Access-Control-Allow-Origin', 'https://www.baidu.com')
  // 查找账户是否已经创建
  // 参数是不是对的
  if(!req.body.username && !req.body.password) {
    rsp_data.
    res.send(httpCom.getRsp(100))
    res.end()
    return
  }
  
  // 账号是否存在
  const data = await mongoPromise.findOne(User, { username: req.body.username }).catch((err) => {
    console.error(err)
    res.send(httpCom.getRsp(1))
    res.end()
  })
  
  // 写入数据库
  if(!data){
    req.body.password = Auth.getPassw(req.body.password)
    const resData = await mongoPromise.insert(User, req.body).catch( err => {
      console.error(err)
      res.send(httpCom.getRsp(1))
    })
    if(resData) res.send(httpCom.getRsp(200))
    
  } else {
    res.send(httpCom.getRsp(100,{},'"' + req.body.username + '"已存在！'))
  }
  res.end()
})

// 登录
router.post('/login',async function(req, res, next) {
  // console.log(Auth.getPassw(req.body.password))
  const data = await mongoPromise.findOne(User, { username: req.body.username, password: Auth.getPassw(req.body.password) }).catch((err) => {
    console.error(err)
    res.send(httpCom.getRsp(1))
    res.end()
  })
  if(data) {
    // 重新生成 session
    req.session.regenerate((err)=>{
      console.error(err)
    })
    // 存储 登录信息
    req.session.userinfo = data.username
    req.session.cip = req.ip
    // 返回token
    res.send(httpCom.getRsp(200,{token: req.session.id}))
    
  } else {
    res.send(httpCom.getRsp(201))
  }
  res.end()
})

router.post('/logout',async function(req, res, next) {
  req.session.destroy((err) => {
    console.error(err)
  })
  res.send(httpCom.getRsp(200))
  // 如果是token怎么处理呢？
  res.end()
})


module.exports = router
