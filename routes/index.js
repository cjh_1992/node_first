var express = require('express')
const rsp = require('../modules/httpCommon')
// const { linkMongo } = require('../modules/mongo/index');
var router = express.Router()

/* GET home page. */
router.all('*', function(req, res, next) {
  // linkMongo()
  // res.render('index', { title: 'Express' })
  console.log('auth')
  if (req.session.userinfo) {
    next()
  } else {
    res.send(rsp.getRsp(102))
    res.end()
  }
})
router.all('/index', function(req, res, next) {
  // linkMongo()
  // res.render('index', { title: 'Express' })
  console.log('auth')
})

module.exports = router
