const crypto = require('crypto')
const token = 'NewP5lpd4e5s'// 加密的密钥；不要动
const hash = crypto.createHash('sha256', token)

// 密码加密
exports.getPassw = function(content) {
  hash.update(content)
  return hash.digest('hex')
}
