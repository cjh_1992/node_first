exports.update = function(model, pramas, data = {}) {
  return new Promise((resolve, reject) => {
    model.update(pramas, data, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

exports.insert = function(Model, data = {}) {
  const mod = new Model(data)
  return new Promise((resolve, reject) => {
    mod.save(function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

exports.remove = function(Model, data = {}) {
  return new Promise((resolve, reject) => {
    Model.remove(data, function(err, res) {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

exports.find = function(Model, data = {}) {
  return new Promise((resolve, reject) => {
    console.log(Model.findOne)
    Model.find(data, function(err, res) {
      console.log(err, data)
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

exports.findOne = function(Model, data = {}) {
  return new Promise((resolve, reject) => {
    Model.findOne(data, function(err, res) {
      console.log(err, data)
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

