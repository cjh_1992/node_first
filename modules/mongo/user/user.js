/*
* 用户表
*/
var mongoose = require('../index.js')
const validation = require('../validation/index')
var Schema = mongoose.Schema

var UserSchema = new Schema({
  name: { type: String, maxLength: 10, validation: validation.userString() },
  username: { type: String },
  password: { type: String },
  userage: { type: Number },
  logindate: { type: Date },
  createDate: { type: Date }
})
const User = mongoose.model('User', UserSchema)
User.on('error', function(err) {
  console.log(err)
})
exports.User = User

