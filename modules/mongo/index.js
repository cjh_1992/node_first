// var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose')
var url = 'mongodb://localhost:27017/test'
mongoose.connect(url, {
  config: { autoIndex: false }, // 关闭自动排序
  useNewUrlParser: true
},
function(err, db) {
  if (err) {
    console.error(err)
    throw err
  }
  console.log('数据库已创建!')
  // db.close()
})
/**
 * 连接异常
 */
mongoose.connection.on('error', function(err) {
  console.log('link ERROR: ' + err)
})

/**
 * 连接断开
 */
mongoose.connection.on('disconnected', function() {
  console.log('Mongoose connection disconnected')
})

module.exports = mongoose
