exports.userString = function() {
  return {
    validator: function(v) {
      return /^\d{11}$/.test(v) || /^(\d|\w|\.)+@(\w|\d)+\.(\w){1,10}$/.test(v)
    },
    message: '"{PATH}"不是手机号或邮箱！'
  }
}

