const msgs = {
  201: '账号或密码错误',
  200: 'success',
  102: '会话已过期，请重新登录',
  101: '没有权限操作',
  100: '参数错误',
  1: '未知的错误'
}
exports.getRsp = function(code, data, msg) {
  return {
    status: code,
    msg: msg || msgs[code] || msgs['200'],
    data: data || data
  }
}
